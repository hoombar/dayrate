package net.rdyonline.dayrate;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Ben Pearson (RdyDev) on 29/04/15.
 */
public class QuarterHourTest {

    @Test
    public void shouldReturnMillisecondFromValue() {
        long oneHour = 1 * 60 * 60 * 1000;
        assertThat(QuarterHour.one.getValue()).isEqualTo(oneHour);
    }

    @Test
    public void setValueShouldMatchGetValue() {
        QuarterHour one = QuarterHour.one;
        QuarterHour sut = QuarterHour.fromValue(one.getValue());

        assertThat(sut.toString()).isEqualTo(one.toString());
    }

}
