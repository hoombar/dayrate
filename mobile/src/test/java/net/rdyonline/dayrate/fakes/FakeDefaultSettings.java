package net.rdyonline.dayrate.fakes;

import net.rdyonline.dayrate.QuarterHour;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.settings.Settings;

import org.joda.time.DateTime;

/**
 * Created by Ben Pearson (RdyDev) on 16/09/15.
 */
public class FakeDefaultSettings implements Settings {

    @Override
    public double getRate() {
        return 0;
    }

    @Override
    public boolean getNotifications() {
        return false;
    }

    @Override
    public Time getStart() {
        return new Time(DateTime.now());
    }

    @Override
    public QuarterHour getLunch() {
        return QuarterHour.one;
    }

    @Override
    public Time getEnd() {
        return new Time(DateTime.now());
    }

    @Override
    public double getOvertimeRate() {
        return 1.5;
    }

    @Override
    public boolean getOvertime() {
        return true;
    }
}
