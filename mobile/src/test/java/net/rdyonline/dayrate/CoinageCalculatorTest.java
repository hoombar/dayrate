package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Ben Pearson (RdyDev) on 25/04/15.
 */
public class CoinageCalculatorTest {

    Time time = mock(Time.class);
    Schedule schedule = mock(Schedule.class);
    DayProgress dayProgress = mock(DayProgress.class);
    Rate rate = mock(Rate.class);

    final int HOURS = 8;
    final double DAY_RATE = 400d;
    final double OVERTIME_RATE = 1.5d;

    CoinageCalculator sut = new CoinageCalculator(schedule, dayProgress, rate);

    @Before
    public void setup() {
        when(rate.perHour(true)).thenReturn(DAY_RATE / HOURS);
        when(rate.getOvertime()).thenReturn(OVERTIME_RATE);
    }

    @Test
    public void shouldReportPennies() {
        long totalSeconds = HOURS * 60 * 60;      // 8 hours
        int secondsSoFar = (HOURS / 2) * 60 * 60;   // 4 hours

        BigDecimal secondRate = new BigDecimal(DAY_RATE).divide(new BigDecimal(totalSeconds), MathContext.DECIMAL128);
        double pennies = secondRate.multiply(new BigDecimal(secondsSoFar)).doubleValue();

        Duration duration = new Duration(totalSeconds * 1000);

        when(schedule.getWorkingDuration(false)).thenReturn(duration);
        when(dayProgress.getSecondsWorked(any(DateTime.class))).thenReturn(secondsSoFar);
        when(rate.perHour(false)).thenReturn(secondRate.multiply(new BigDecimal(60 * 60)).doubleValue());

        assertThat(sut.getCurrentCoins(new Time(0, 0).toDateTime())).isEqualTo(pennies);
    }

    @Test
    public void overTimeCoinsShouldConsiderRate() {
        DateTime current = DateTime.now();
        long additionalSeconds = 61 * 60;
        when(dayProgress.getOvertimeSeconds(current)).thenReturn(additionalSeconds);

        double perSecond = 400d / 8d / 60d / 60d;
        double value = additionalSeconds * perSecond * 1.5;
        assertThat(sut.getOvertimeCoins(current)).isEqualTo(value);
    }

    @Test
    public void shouldZeroOnNegativeValue() {
        DateTime current = DateTime.now();
        long additionalSeconds = 0;
        when(dayProgress.getOvertimeSeconds(current)).thenReturn(additionalSeconds);
        assertThat(sut.getOvertimeCoins(current)).isEqualTo(0);
    }

}
