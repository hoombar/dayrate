package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class DayProgressTest {

    Time time = new Time(DateTime.now());
    long lunch = 1 * 60 * 60 * 1000;
    Schedule schedule;

    DayProgress sut;

    @Before
    public void setup() {
        schedule = spy(new Schedule(time, lunch, time));

        // defaults
        doReturn(new Time(1, 0)).when(schedule).getStartTime();
        doReturn(new Time(4, 0)).when(schedule).getEndTime();

        sut = new DayProgress(schedule);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowNullArgs() {
        schedule = null;

        sut = new DayProgress(schedule);
    }

    @Test
    public void shouldAccountForZeroHour() {
        doReturn(new Time(0, 0)).when(schedule).getStartTime();
        doReturn(new Time(1, 0)).when(schedule).getEndTime();

        Time now = new Time(0, 30);
        double progress = sut.getProgress(now.toDateTime());

        assertThat(progress).isEqualTo(50);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptInvalidArgs() {
        sut = new DayProgress(null);

        fail("IllegalArgumentException should have been thrown");
    }

    @Test
    public void shouldReportZeroWhenDayNotStarted() {
        Time now = new Time(0, 0);
        double progress = sut.getProgress(now.toDateTime());

        assertThat(progress).isEqualTo(0);
    }

    @Test
    public void shouldReportOneHundredWhenDayFinished() {
        Time now = new Time(5, 0);
        double progress = sut.getProgress(now.toDateTime());

        assertThat(progress).isEqualTo(100);
    }

    @Test
    public void shouldReportInProgressWhenStarted() {
        int hour = schedule.getStartTime().getHour();
        int minute = schedule.getStartTime().getMinute() + 1;
        Time justStarted = new Time(hour, minute);
        boolean started = sut.hasDayStarted(justStarted);

        assertThat(started).isTrue();
    }

    @Test
    public void shouldReportNotInProgressBeforeStart() {
        int hour = schedule.getStartTime().getHour();
        int minute = schedule.getStartTime().getMinute() - 1;
        Time nearlyStarted = new Time(hour, minute);

        assertThat(sut.hasDayStarted(nearlyStarted)).isFalse();
    }

    @Test
    public void shouldReportFiftyPercentWhenHalfWay() {
        doReturn(new Time(8, 50)).when(schedule).getStartTime();
        doReturn(new Time(8, 54)).when(schedule).getEndTime();

        Time middle = new Time(8, 52);

        double progress = sut.getProgress(middle.toDateTime());

        assertThat(progress).isEqualTo(50);
    }

    /***
     * Typically, new Time(8,52).toDateTime would be used,
     * but in instances where a long is being passed through from a calendar instance, a check needs
     * to be made that it isn't breaking due to the discrepancy in years/months
     */
    @Test
    public void shouldReportProgressWhenCurrentDateUsed() {
        doReturn(new Time(8, 50)).when(schedule).getStartTime();
        doReturn(new Time(8, 54)).when(schedule).getEndTime();

        DateTime dateTime = new DateTime(2015, 1, 1, 8, 52, 0); // 2015-01-01:08:52:00
        double progress = sut.getProgress(dateTime);

        assertThat(progress).isEqualTo(50);
    }

    @Test
    public void getSecondsWorkedNotShouldAccountForLunch() {
        int secondsWorked = 2 * 60 * 60;
        int secondsWorkedIncLunch = secondsWorked + (60 * 60);

        assertThat(sut.getSecondsWorked(new Time(4, 0).toDateTime())).isEqualTo(secondsWorkedIncLunch);
    }

    @Test
    public void getSecondsWorkedShouldNotExceedMaxDay() {
        int secondsWorked = 2 * 60 * 60;
        doReturn(new Time(1, 0)).when(schedule).getStartTime();
        doReturn(new Time(3, 0)).when(schedule).getEndTime();

        assertThat(sut.getSecondsWorked(new Time(12, 0).toDateTime())).isEqualTo(secondsWorked);
    }

    @Test
    public void secondsWorkedShouldReportZeroWhenTimeBeforeStartTime() {
        doReturn(new Time(1, 0)).when(schedule).getStartTime();
        doReturn(new Time(3, 0)).when(schedule).getEndTime();

        assertThat(sut.getSecondsWorked(new Time(0, 0).toDateTime())).isEqualTo(0);
    }

}
