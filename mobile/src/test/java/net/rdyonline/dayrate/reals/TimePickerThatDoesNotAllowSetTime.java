package net.rdyonline.dayrate.reals;

import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.settings.dialog.TimePickerDialogPreference;

import android.content.Context;

/**
 * Created by Ben Pearson (RdyDev) on 26/11/15.
 */
public class TimePickerThatDoesNotAllowSetTime extends TimePickerDialogPreference {

    public TimePickerThatDoesNotAllowSetTime(final Context context) {
        super(context);
    }

    @Override
    protected boolean setTime(final Time newValue) {
        return false;
    }

    @Override
    protected Time getTime() {
        return null;
    }
}
