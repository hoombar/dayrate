package net.rdyonline.dayrate.reals;

import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.settings.SettingsValidationException;
import net.rdyonline.dayrate.settings.dialog.TimePickerDialogPreference;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Ben Pearson (RdyDev) on 26/11/15.
 */
public class TimePickerThatThrowsExOnSetTime extends TimePickerDialogPreference {

    public TimePickerThatThrowsExOnSetTime(final Context context) {
        super(context);
    }

    @Override
    protected boolean setTime(final Time newValue) {
        throw new SettingsValidationException("");
    }

    @Override
    protected Time getTime() {
        return null;
    }

}
