package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class TimeTest {

    final int HOUR = 23;
    final int MINUTE = 0;

    Time sut = new Time(23, 0);

    @Test
    public void toDateTimeShouldReturnValidDateTime() {
        DateTime test = new DateTime(1970, 1, 1, 23, 0, 0);
        DateTime result = sut.toDateTime();

        assertThat(result).isEqualTo(test);
    }

    @Test
    public void dateAfterShouldReturnTrue() {
        Time early = new Time(8, 0);
        Time late = new Time(10, 0);

        assertThat(late.isAfter(early)).isTrue();
    }

    @Test
    public void dateBeforeShouldReturnTrue() {
        Time early = new Time(8, 0);
        Time late = new Time(10, 0);

        assertThat(early.isBefore(late)).isTrue();
    }

    @Test
    public void shouldNotEqualIfDifferentMinute() {
        sut = new Time(0, 1, 0);
        Time time = new Time(0, 2, 0);

        assertThat(sut).isNotEqualTo(time);
    }

    @Test
    public void shouldNotEqualIfDifferentHour() {
        sut = new Time(1, 0, 0);
        Time time = new Time(2, 0, 0);

        assertThat(sut).isNotEqualTo(time);
    }

    @Test
    public void shouldNotEqualIfDifferentSecond() {
        sut = new Time(0, 0, 1);
        Time time = new Time(0, 0, 2);

        assertThat(sut).isNotEqualTo(time);
    }

    @Test
    public void shouldEqualWhenSame() {
        Time time1 = new Time(1, 2, 3);
        Time time2 = new Time(1, 2, 3);

        assertThat(time1).isEqualTo(time2);
    }
}
