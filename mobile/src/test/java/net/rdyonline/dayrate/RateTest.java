package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Ben Pearson (RdyDev) on 24/04/15.
 */
public class RateTest {

    float dayPay = 450.00f;
    Schedule schedule = mock(Schedule.class);

    Rate sut = new Rate(dayPay, schedule, 1.5f);

    @Test
    public void getDayShouldReturn450() {
        assertThat(sut.perDay()).isEqualTo(dayPay);
    }

    @Test
    public void getHourShouldReturn450div7() {
        double expected = 450.00d / 7d;
        DateTime oneAm = new DateTime(1970, 1, 1, 1, 0, 0);
        DateTime eightAm = new DateTime(1970, 1, 1, 8, 0, 0);
        Duration duration = new Duration(oneAm, eightAm);

        when(schedule.getWorkingDuration(false)).thenReturn(duration);

        assertThat(sut.perHour(false)).isCloseTo(expected, offset(0.00000000000002));
    }

    @Test
    public void getMinuteShouldReturn450div7div60() {
        double expected = 450.00d / (7d * 60);
        DateTime oneAm = new DateTime(1970, 1, 1, 1, 0, 0);
        DateTime eightAm = new DateTime(1970, 1, 1, 8, 0, 0);
        Duration duration = new Duration(oneAm, eightAm);

        when(schedule.getWorkingDuration(false)).thenReturn(duration);

        assertThat(sut.perMinute(false)).isEqualTo(expected);
    }

    @Test
    public void getSecondShouldReturn450div7div60div60() {
        double expected = 450.00d / (7d * (60d * 60d));
        DateTime oneAm = new DateTime(1970, 1, 1, 1, 0, 0);
        DateTime eightAm = new DateTime(1970, 1, 1, 8, 0, 0);
        Duration duration = new Duration(oneAm, eightAm);

        when(schedule.getWorkingDuration(false)).thenReturn(duration);

        assertThat(sut.perSecond(false)).isEqualTo(expected);
    }
}
