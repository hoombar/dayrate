package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Ben Pearson (RdyDev) on 13/05/15.
 */
public class OvertimeProgressTest {

    // mocks
    Schedule baseSchedule = mock(Schedule.class);
    // reals
    Time startTime = new Time(10, 0);
    Time endTime = new Time(22, 0);
    Duration durationWithoutLunch = new Duration(1000 * 60 * 60 * 12);

    OvertimeProgress sut = new OvertimeProgress(baseSchedule);

    @Before
    public void setup() {
        when(baseSchedule.getStartTime()).thenReturn(startTime);
        when(baseSchedule.getEndTime()).thenReturn(endTime);
        when(baseSchedule.getWorkingDuration(true)).thenReturn(durationWithoutLunch);
    }

    @Test
    public void progressShouldBeZeroBeforeWorkingDayFinished() {
        int progress = sut.getProgress(0);
        assertThat(progress).isEqualTo(0);
    }

    @Test
    public void progressShouldBeOneHundredWhenExtraDayWorked() {
        // the duration of overtime should map against how long a normal day is
        long millisForWorkingDay = durationWithoutLunch.getMillis();
        DateTime workingDay = new DateTime(millisForWorkingDay);
        DateTime endOfOvertime = workingDay.plus(durationWithoutLunch.getMillis());
        long elapsedSeconds = (endOfOvertime.getMillis() - workingDay.getMillis()) / 1000;

        assertThat(sut.getProgress(elapsedSeconds)).isEqualTo(100);
    }

    @Test
    public void shouldBeOneHundredWhenOvertimeExceeded() {
        long millisForWorkingDay = durationWithoutLunch.getMillis();
        DateTime workingDay = new DateTime(millisForWorkingDay);
        DateTime endOfOvertime = workingDay.plus((durationWithoutLunch.getMillis() / 4) * 5);

        assertThat(sut.getProgress(999999999)).isEqualTo(100);
    }

}
