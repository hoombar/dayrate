package net.rdyonline.dayrate.settings.validation;

import android.content.Context;

import net.rdyonline.dayrate.BuildConfig;
import net.rdyonline.dayrate.settings.SettingsValidationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by Ben Pearson (RdyDev) on 12/05/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class RateValidationTest {

    private static final int LOWEST_VALUE = 1;
    private static final int HIGHEST_VALUE = 49999;

    Context context = RuntimeEnvironment.application;
    RateValidation sut = new RateValidation(context);

    @Test(expected = SettingsValidationException.class)
    public void shouldNotAllowZeroRate() {
        int zero = 0;
        sut.isValid(zero);

        fail("Should not allow validation on lowest value");
    }

    @Test
    public void shouldAllowRateAtLowerRangeBound() {
        assertThat(sut.isValid(LOWEST_VALUE)).isTrue();
    }

    @Test
    public void shouldAllowRateAtUpperRangeBound() {
        assertThat(sut.isValid(HIGHEST_VALUE)).isTrue();
    }

    @Test
    public void shouldNotAllowRateAtUpperBound() {
        assertThat(sut.isValid(HIGHEST_VALUE + 1)).isFalse();
    }

    @Test(expected = SettingsValidationException.class)
    public void shouldNotAllowRateBelowLowestBound() {
        sut.isValid(LOWEST_VALUE - 1);

        fail("Should not allow validation on lowest value");
    }
}
