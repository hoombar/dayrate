package net.rdyonline.dayrate.settings.dialog;

import net.rdyonline.dayrate.BuildConfig;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.modules.ApplicationModule;
import net.rdyonline.dayrate.reals.TimePickerThatDoesNotAllowSetTime;
import net.rdyonline.dayrate.reals.TimePickerThatThrowsExOnSetTime;
import net.rdyonline.dayrate.settings.SavedSettings;
import net.rdyonline.dayrate.settings.Settings;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.content.Context;
import android.preference.Preference;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Ben Pearson (RdyDev) on 22/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class TimePickerDialogPreferenceTest {

    @Mock
    Preference mockStartPreference;
    @Mock
    Preference mockEndPreference;
    @Mock
    SavedSettings mockSavedSettings;

    private Context mContext;

    Settings.OnValidationError errorListener = spy(new Settings.OnValidationError() {
        @Override
        public void error(final String message) {

        }
    });

    private Time startTime = Time.fromMillis(100000);
    private Time endTime = Time.fromMillis(1000000);

    private TimePickerDialogPreference timePickerDialogPreference;

    @Before
    public void setup() {
        mContext = RuntimeEnvironment.application.getApplicationContext();
        MockitoAnnotations.initMocks(this);
        ApplicationModule.init(RuntimeEnvironment.application.getApplicationContext());

        when(mockStartPreference.getKey()).thenReturn("KEY_START");
        when(mockEndPreference.getKey()).thenReturn("KEY_END");

        when(mockSavedSettings.getStart()).thenReturn(startTime);
        when(mockSavedSettings.getEnd()).thenReturn(endTime);
    }

    @Test
    public void shouldNotAllowStartSetAfterEnd() {
        useValidationFailTimePicker();
        timePickerDialogPreference.onPreferenceChange(mockStartPreference, 110l);

        verify(errorListener).error(anyString());
    }

    private void useFalseTimePicker() {
        timePickerDialogPreference = new TimePickerThatDoesNotAllowSetTime(mContext);
        timePickerDialogPreference.setErrorListener(errorListener);
    }

    private void useValidationFailTimePicker() {
        timePickerDialogPreference = new TimePickerThatThrowsExOnSetTime(mContext);
        timePickerDialogPreference.setErrorListener(errorListener);
    }

}