package net.rdyonline.dayrate.settings;

import net.rdyonline.dayrate.BuildConfig;
import net.rdyonline.dayrate.QuarterHour;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.fakes.FakeDefaultSettings;
import net.rdyonline.dayrate.settings.validation.EndTimeValidation;
import net.rdyonline.dayrate.settings.validation.OvertimeRateValidation;
import net.rdyonline.dayrate.settings.validation.RateValidation;
import net.rdyonline.dayrate.settings.validation.StartTimeValidation;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowPreferenceManager;

import android.content.Context;
import android.content.SharedPreferences;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Ben Pearson (RdyDev) on 28/04/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class SavedSettingsTest {

    @Mock
    private RateValidation mockRateValidation;
    @Mock
    private StartTimeValidation mockStartTimeValidation;
    @Mock
    private EndTimeValidation mockEndTimeValidation;
    @Mock
    private OvertimeRateValidation mockOvertimeRateValidation;
    SharedPreferences sharedPreferences;

    private Context context = RuntimeEnvironment.application;
    private Settings fakeDefaultSettings = new FakeDefaultSettings();

    private SavedSettings sut;


    @Before
    public void setup() {
        DateTimeUtils.setCurrentMillisFixed(DateTime.parse("2004-06-09T16:30:30Z").getMillis());

        MockitoAnnotations.initMocks(this);
        sharedPreferences = spy(ShadowPreferenceManager.getDefaultSharedPreferences(RuntimeEnvironment.application.getApplicationContext()));
        when(mockEndTimeValidation.isValid(any(SavedSettings.class), any(Time.class))).thenReturn(true);

        sut = spy(new SavedSettings(context, mockRateValidation, mockOvertimeRateValidation, fakeDefaultSettings, sharedPreferences, mockStartTimeValidation, mockEndTimeValidation));
    }

    @Test
    public void shouldPopulateDefaults() {
        // called in the constructor, but can't spy on execution logic in constructor, so called again
        sut.setDefaultsIfBlank();

        verify(sut).setStart(fakeDefaultSettings.getStart());
        verify(sut).setLunch(fakeDefaultSettings.getLunch());
        verify(sut).setEnd(fakeDefaultSettings.getEnd());
        verify(sut).setRate(fakeDefaultSettings.getRate());
        verify(sut).setNotifications(fakeDefaultSettings.getNotifications());
    }

    @Test
    public void shouldNotSetDefaultsIfAlreadySet() {
        // called in the constructor, but can't spy on execution logic in constructor, so called again
        sut.setDefaultsIfBlank();
        sut.setDefaultsIfBlank();
        sut.setDefaultsIfBlank();
        sut.setDefaultsIfBlank();

        verify(sut, times(1)).setStart(fakeDefaultSettings.getStart());
        verify(sut, times(1)).setLunch(fakeDefaultSettings.getLunch());
        verify(sut, times(1)).setEnd(fakeDefaultSettings.getEnd());
        verify(sut, times(1)).setRate(fakeDefaultSettings.getRate());
        verify(sut, times(1)).setNotifications(fakeDefaultSettings.getNotifications());
    }

    @Test
    public void getStartShouldReturnSetStartValue() {
        Time time = new Time(9, 0, 0);
        sut.setStart(time);
        Time result = sut.getStart();

        assertThat(result == time);
    }

    @Test
    public void getEndShouldReturnSetEndValue() {
        Time time = new Time(0, 0, 0);
        assertThat(sut.setEnd(time)).isTrue();
        Time result = sut.getEnd();

        assertThat(result.toMillis()).isEqualTo(time.toMillis());
    }

    @Test
    public void lunchShouldSave() {
        long oneHour = 1 * 60 * 60 * 1000;

        QuarterHour quarterHour = QuarterHour.one;
        sut.setLunch(quarterHour);

        assertThat(sut.getLunch().getValue()).isEqualTo(oneHour);
    }

    @Test
    public void shouldNotShowNotificationIfDismissedToday() {
        sut.setNotificationDismissed(DateTime.now().minusHours(1));
        doReturn(true).when(sharedPreferences).getBoolean(sut.KEY_NOTIFICATIONS, fakeDefaultSettings.getNotifications());

        assertThat(sut.getNotifications()).isFalse();
    }

    @Test
    public void shouldCheckNotificationDismissByDayAndNotHour() {
        sut.setNotificationDismissed(DateTime.now().minusHours(23));
        doReturn(true).when(sharedPreferences).getBoolean(sut.KEY_NOTIFICATIONS, fakeDefaultSettings.getNotifications());

        assertThat(sut.getNotifications()).isTrue();
    }

    @Test
    public void notificationDismissShouldBeTrueIfYesterday() {
        sut.setNotificationDismissed(DateTime.now().minusHours(25));
        doReturn(true).when(sharedPreferences).getBoolean(sut.KEY_NOTIFICATIONS, fakeDefaultSettings.getNotifications());

        assertThat(sut.getNotifications()).isTrue();
    }

    @Test
    public void shouldNotShowNotificationWhenFalseInSettingsEvenIfDismissedMoreThanOneDayAgo() {
        sut.setNotificationDismissed(DateTime.now().minusHours(1));
        doReturn(false).when(sharedPreferences).getBoolean(sut.KEY_NOTIFICATIONS, fakeDefaultSettings.getNotifications());

        assertThat(sut.getNotifications()).isFalse();
    }
}
