package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class ScheduleTest {

    private Time startTime = mock(Time.class);
    private long lunch = 1 * 60 * 60 * 1000; // 1 hour in millis
    private Time endTime = mock(Time.class);

    private Schedule sut;

    @Before
    public void setup() {
        setMockTime(startTime, 2, 0);
        setMockTime(endTime, 5, 0);
        when(startTime.compareTo(endTime)).thenReturn(-1);

        sut = new Schedule(startTime, lunch, endTime);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowStartTimeAfterEndTime() {
        when(startTime.compareTo(endTime)).thenReturn(1);

        sut = new Schedule(endTime, lunch, startTime);

        fail("start time can not be after end time");
    }

    @Test
    public void totalWorkHoursShouldIncludeLunch() {
        setMockTime(startTime, 2, 0);
        setMockTime(endTime, 5, 0);

        float workingHours = sut.getWorkingDuration(false).getStandardHours();

        assertThat(workingHours).isEqualTo(3.0f);
    }

    private void setMockTime(Time mock, int hour, int minute) {
        when(mock.getHour()).thenReturn(hour);
        when(mock.getMinute()).thenReturn(minute);
        when(mock.toDateTime()).thenReturn(new DateTime(1970, 1, 1, hour, minute, 0));
    }
}
