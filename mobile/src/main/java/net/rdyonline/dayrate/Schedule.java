package net.rdyonline.dayrate;

import net.rdyonline.dayrate.settings.SavedSettings;

import org.joda.time.Duration;

/**
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class Schedule {

    private Time startTime;
    // milliseconds
    private long lunchDuration;
    private Time endTime;

    public Schedule(SavedSettings savedSettings) {
        this(savedSettings.getStart(), savedSettings.getLunch().getValue(), savedSettings.getEnd());
    }

    /**
     * @param startTime     when the day starts
     * @param lunchDuration in milliseconds
     * @param endTime       until the day ends
     */
    public Schedule(Time startTime, long lunchDuration, Time endTime) {
        if (startTime.compareTo(endTime) > 0) {
            throw new IllegalArgumentException("end time can not be before start time");
        }

        this.startTime = startTime;
        this.lunchDuration = lunchDuration;
        this.endTime = endTime;
    }

    public Time getStartTime() {
        return startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public Duration getLunch() {
        return new Duration(lunchDuration);
    }

    public Duration getWorkingDuration(boolean excludeLunch) {
        Duration workingDay = new Duration(getStartTime().toDateTime(), getEndTime().toDateTime());
        if (excludeLunch) {
            workingDay = workingDay.minus(getLunch());
        }

        return workingDay;
    }

    @Override
    public String toString() {
        return startTime.asString() + " - " + endTime.asString();
    }
}
