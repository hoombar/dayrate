package net.rdyonline.dayrate;

import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 * Created by Ben Pearson (RdyDev) on 22/04/15.
 */
public class DayProgress {

    private final Schedule schedule;

    /**
     * The {@link net.rdyonline.dayrate.DayProgress} allows you to easily report back on how far through
     * the day you are
     *
     * @param schedule
     */
    public DayProgress(Schedule schedule) throws IllegalArgumentException {
        if (schedule == null) {
            throw new IllegalArgumentException("schedule can not be null");
        }

        this.schedule = schedule;
    }

    /**
     * Work out how far through the day we are
     *
     * @return progress between 0 and 100
     */
    public final int getProgress(DateTime now) {
        Time currentTime = new Time(now);
        int secondsWorked = getSecondsWorked(now);

        // if the day hasn't started, return 0
        if (!hasDayStarted(currentTime)) return 0;

        // if the day has finished, return 100
        if (hasDayFinished(currentTime)) {
            return 100;
        }

        long totalSeconds = schedule.getWorkingDuration(false).getStandardSeconds();

        if (secondsWorked > totalSeconds) return 100;

        return (int) (((float) 100 / (float) totalSeconds) * secondsWorked);
    }

    public int getSecondsWorked(DateTime time) {
        Time simpleTime = new Time(time);

        DateTime start = schedule.getStartTime().toDateTime();
        Time endTime;
        if (simpleTime.isAfter(schedule.getEndTime())) {
            endTime = schedule.getEndTime();
        } else {
            endTime = simpleTime;
        }

        Duration workedDuration = new Duration(start.toDateTime(), endTime.toDateTime());
        int secondsWorked = (int) workedDuration.getStandardSeconds();

        return secondsWorked < 0 ? 0 : secondsWorked;
    }

    /***
     * How many seconds have elapsed since overtime started
     *
     * @param dateTime current {@link DateTime} value
     * @return seconds elapsed
     */
    public long getOvertimeSeconds(DateTime dateTime) {
        Time endTime = schedule.getEndTime();
        DateTime overtimeStart = endTime.toDateTime();

        // the start time will have a 1970 start, so update the DateTime to the current y/m/d
        int currentYear = dateTime.getYear();
        int currentMonth = dateTime.getMonthOfYear();
        int currentDay = dateTime.getDayOfMonth();
        overtimeStart = overtimeStart.withDate(currentYear, currentMonth, currentDay);

        long elapsed = (dateTime.getMillis() - overtimeStart.getMillis()) / 1000;

        return elapsed;
    }

    public boolean hasDayStarted(Time now) {
        Time start = schedule.getStartTime();

        return now.compareTo(start) > 0;
    }

    public boolean hasDayFinished(Time now) {
        Time end = schedule.getEndTime();

        return now.compareTo(end) > 0;
    }

}
