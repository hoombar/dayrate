package net.rdyonline.dayrate;

/**
 * Lunch is set using 1/4 of an hour intervals
 * <p/>
 * Created by Ben Pearson (RdyDev) on 29/04/15.
 */
public enum QuarterHour {

    quart(15, "¼"),
    half(30, "½"),
    threeQuart(45, "¾"),
    one(60, "1"),
    oneAndQuart((60 + 15), "1 ¼"),
    oneAndHalf((60 + 30), "1 ½"),
    oneAndThreeQuart((60 + 45), "1 ¾"),
    two(120, "2"),
    twoAndQuart((120 + 15), "2 ¼"),
    twoAndHalf((120 + 30), "2 ½"),
    twoAndThreeQuart((120 + 45), "2 ¾");

    private static final int MINUTE = 60 * 1000;

    private long value;
    private String string;

    QuarterHour(long value, String string) {
        this.value = value;
        this.string = string;
    }

    public long getValue() {
        return value * MINUTE;
    }

    @Override
    public String toString() {
        return string;
    }

    public static QuarterHour fromValue(long value) {
        for (QuarterHour quarterHour : QuarterHour.values()) {
            if (quarterHour.getValue() == value) {
                return quarterHour;
            }
        }

        return null;
    }

    public static QuarterHour fromString(String string) {
        for (QuarterHour quarterHour : QuarterHour.values()) {
            if (quarterHour.toString().equals(string)) {
                return quarterHour;
            }
        }

        return null;
    }

    public static final String increments[] = {
            quart.string, half.string, threeQuart.string, one.string,
            oneAndQuart.string, oneAndHalf.string, oneAndThreeQuart.string, two.string,
            twoAndQuart.string, twoAndHalf.string, twoAndThreeQuart.string
    };

}
