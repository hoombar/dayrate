package net.rdyonline.dayrate;

import net.rdyonline.dayrate.modules.ApplicationModule;
import net.rdyonline.dayrate.notification.NotificationReceiver;

import android.app.Application;
import android.content.IntentFilter;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Ben Pearson (RdyDev) on 16/09/15.
 */
public class DayRateApplication extends Application {

    public static final String DISMISS_NOTIFICATION = "dismissNotification";;
    public static final String BABAS_NEUE_REGULAR_TTF = "fonts/BabasNeue-Regular.ttf";

    @Override
    public void onCreate() {
        super.onCreate();

        ApplicationModule.init(this);

        // intent filter for dismissing notification
        // add this here rather than in the app manifest so that any testing frameworks (Robolectric) don't trigger the service
        // and cause any crashes from injected dependencies that have the wrong context associated
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DISMISS_NOTIFICATION);
        registerReceiver(new NotificationReceiver(), intentFilter);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(BABAS_NEUE_REGULAR_TTF)
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
