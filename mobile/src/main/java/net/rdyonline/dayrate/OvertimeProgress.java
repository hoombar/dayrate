package net.rdyonline.dayrate;

/**
 * Created by Ben Pearson (RdyDev) on 13/05/15.
 */
public class OvertimeProgress {

    private final Schedule schedule;

    public OvertimeProgress(Schedule schedule) {
        this.schedule = schedule;
    }

    /***
     * Working out the progress for overtime is a little more tricky than working it out for
     * a day. If the overtime spills over to the next day, you can no longer compare two
     * {@link Time} objects, as the later value would be before the first (as there is no concept
     * of date/day. As a result, the sensible conclusion is that ticks should be measured instead
     * of comparing two Time objects
     *
     * @param elapsedSeconds how many seconds have passed since the day end time
     * @return a valid progress range between 0 - 100
     */
    public int getProgress(long elapsedSeconds) {
        if (elapsedSeconds <= 0) {
            return 0;
        }

        long totalSeconds = schedule.getWorkingDuration(true).getStandardSeconds();

        int result = (int) (((float) 100 / (float) totalSeconds) * elapsedSeconds);

        return (result >= 100) ? 100 : result;
    }
}
