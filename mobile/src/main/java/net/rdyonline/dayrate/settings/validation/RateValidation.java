package net.rdyonline.dayrate.settings.validation;

import android.content.Context;

import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.settings.SettingsValidationException;

/**
 * Created by Ben Pearson (RdyDev) on 12/05/15.
 */
public class RateValidation {

    public static final int LOWER_BOUND = 0;
    public static final int UPPER_BOUND = 50000;

    private String messageBelowEqualZero;
    private String messageExcessive;

    public RateValidation(Context context) {
        messageBelowEqualZero = context.getString(R.string.validation_rate_lte_zero);

        String max = context.getString(R.string.validation_rate_gt_max);
        messageExcessive = String.format(max, UPPER_BOUND);
    }

    public boolean isValid(double value) {
        boolean valid = value > LOWER_BOUND && value < UPPER_BOUND;

        if (valid) return true;

        if (value <= LOWER_BOUND) {
            throw new SettingsValidationException(messageBelowEqualZero);
        }

        if (value > UPPER_BOUND) {
            throw new SettingsValidationException(messageExcessive);
        }

        return false;
    }
}
