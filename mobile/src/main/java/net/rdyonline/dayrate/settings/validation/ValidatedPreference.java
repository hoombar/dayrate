package net.rdyonline.dayrate.settings.validation;

import android.preference.Preference;

import net.rdyonline.dayrate.settings.SavedSettings;

/**
 * Created by Ben Pearson (RdyDev) on 21/05/15.
 */
public interface ValidatedPreference extends Preference.OnPreferenceChangeListener {

    void setErrorListener(SavedSettings.OnValidationError listener);
}
