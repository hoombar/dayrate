package net.rdyonline.dayrate.settings.dialog;

import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.settings.SavedSettings;
import net.rdyonline.dayrate.settings.Settings;
import net.rdyonline.dayrate.settings.SettingsValidationException;
import net.rdyonline.dayrate.settings.validation.ValidatedPreference;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;

public abstract class TimePickerDialogPreference extends DialogPreference implements ValidatedPreference {
    private TimePicker picker = null;
    protected Settings.OnValidationError listener;
    protected SavedSettings savedSettings;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TimePickerDialogPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public TimePickerDialogPreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);
        init();
    }

    public TimePickerDialogPreference(Context ctxt, AttributeSet attrs) {
        super(ctxt, attrs);
        init();
    }

    public TimePickerDialogPreference(Context context) {
        this(context, null);
    }

    private void init() {
        this.savedSettings = savedSettings();
        setPositiveButtonText(R.string.set);
        setNegativeButtonText(R.string.cancel);
        setOnPreferenceChangeListener(this);
    }

    @Override
    protected View onCreateDialogView() {
        picker = new TimePicker(getContext());
        return (picker);
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        picker.setCurrentHour(getTime().getHour());
        picker.setCurrentMinute(getTime().getMinute());
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            Time time = new Time(picker.getCurrentHour(), picker.getCurrentMinute());

            setSummary(getSummary());
            if (callChangeListener(time.toMillis())) {
                persistLong(time.toMillis());
                notifyChanged();
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return (a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setSummary(getSummary());
    }

    @Override
    public void setErrorListener(final SavedSettings.OnValidationError listener) {
        this.listener = listener;
    }

    @Override
    public boolean onPreferenceChange(final Preference preference, final Object newValue) {
        Time newTime = Time.fromMillis((long) newValue);

        try {
            return setTime(newTime);
        } catch (SettingsValidationException e) {
            listener.error(e.getFriendlyMessage());
        }

        return false;
    }

    @Override
    protected String getPersistedString(String defaultReturnValue) {
        return Long.toString(getTime().toMillis());
    }

    @Override
    protected boolean persistString(String value) {
        try {
            long time = Long.parseLong(value);
            setTime(Time.fromMillis(time));
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    @Override
    public CharSequence getSummary() {
        return getTime().asString();
    }

    protected abstract boolean setTime(Time newValue);

    protected abstract Time getTime();
}