package net.rdyonline.dayrate.settings.validation;

import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.settings.SavedSettings;
import net.rdyonline.dayrate.settings.SettingsValidationException;

import android.content.Context;

import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;

/**
 * Created by Ben Pearson (RdyDev) on 22/11/15.
 */
public class EndTimeValidation {

    private Context context;

    public EndTimeValidation(Context context) {
        this.context = context;
    }

    public boolean isValid(SavedSettings savedSettings, Time endTime) {
        boolean valid = savedSettings.getStart().isBefore(endTime);

        if (!valid) {
            throw new SettingsValidationException(context.getString(R.string.validation_start_time_end_time));
        }

        return valid;
    }
}
