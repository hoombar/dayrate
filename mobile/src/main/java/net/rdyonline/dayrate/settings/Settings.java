package net.rdyonline.dayrate.settings;

import net.rdyonline.dayrate.QuarterHour;
import net.rdyonline.dayrate.Time;

/**
 * Created by Ben Pearson (RdyDev) on 15/09/15.
 */
public interface Settings {

    double getRate();
    boolean getNotifications();
    Time getStart();
    QuarterHour getLunch();
    Time getEnd();
    double getOvertimeRate();
    boolean getOvertime();

    interface OnValidationError {
        void error(String message);
    }
}
