package net.rdyonline.dayrate.settings;

import net.rdyonline.dayrate.R;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;

/**
 * Created by Ben Pearson (RdyDev) on 27/04/15.
 */
public class SettingsActivity extends AppCompatActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    private SavedSettings proxy;

    public SettingsActivity() {
        this(savedSettings());
    }

    public SettingsActivity(SavedSettings savedSettings) {
        // defaults will be saved to shared preferences if they don't already exist
        proxy = savedSettings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.inject(this);

        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new SettingsFragment())
                .commit();
    }

}
