package net.rdyonline.dayrate.settings;

import net.rdyonline.dayrate.QuarterHour;
import net.rdyonline.dayrate.Time;

import android.content.SharedPreferences;

/**
 * Created by Ben Pearson (RdyDev) on 15/09/15.
 */
public class DefaultSettings implements Settings {

    // defaults
    private final double DEFAULT_RATE = 500.00d;
    private final boolean DEFAULT_NOTIFICATIONS = true;
    // 9am
    private final Time DEFAULT_START = new Time(9, 0, 0);
    // 5pm
    private final Time DEFAULT_END = new Time(17, 30, 0);
    // 1 hour
    private final long DEFAULT_LUNCH_DURATION = 1000 * 60 * 60 * 1;
    private final boolean DEFAULT_OVERTIME = false;
    // x 1.5
    private final float DEFAULT_OVERTIME_RATE = 1.5f;

    @Override
    public double getRate() {
        return DEFAULT_RATE;
    }

    @Override
    public boolean getNotifications() {
        return DEFAULT_NOTIFICATIONS;
    }

    @Override
    public Time getStart() {
        return DEFAULT_START;
    }

    @Override
    public QuarterHour getLunch() {
        return QuarterHour.fromValue(DEFAULT_LUNCH_DURATION);
    }

    @Override
    public Time getEnd() {
        return DEFAULT_END;
    }

    @Override
    public double getOvertimeRate() {
        return DEFAULT_OVERTIME_RATE;
    }

    @Override
    public boolean getOvertime() {
        return DEFAULT_OVERTIME;
    }
}
