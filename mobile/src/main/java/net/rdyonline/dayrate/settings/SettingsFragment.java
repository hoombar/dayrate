package net.rdyonline.dayrate.settings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.notification.NotificationService;
import net.rdyonline.dayrate.settings.validation.ValidatedPreference;

import static net.rdyonline.dayrate.modules.SettingsModule.defaultSettings;
import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;
import static net.rdyonline.dayrate.modules.ValidationModule.overtimeRateValidation;
import static net.rdyonline.dayrate.modules.ValidationModule.rateValidation;

/**
 * Created by Ben Pearson (RdyDev) on 27/04/15.
 */
public class SettingsFragment extends PreferenceFragment implements
        SavedSettings.OnValidationError, SharedPreferences.OnSharedPreferenceChangeListener {

    SavedSettings mSavedSettings;
    ValidatedPreference rate;
    ValidatedPreference overtimeRate;
    ValidatedPreference startTime;
    ValidatedPreference endTime;

    public SettingsFragment() {
        this(savedSettings());
    }

    @SuppressLint("ValidFragment")
    public SettingsFragment(SavedSettings savedSettings) {
        mSavedSettings = savedSettings;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        // load prefs from XML
        addPreferencesFromResource(R.xml.preferences);

        rate = (ValidatedPreference) findPreference(getString(R.string.key_rate));
        overtimeRate = (ValidatedPreference) findPreference(getString(R.string.key_overtime_rate));
        startTime = (ValidatedPreference) findPreference(getString(R.string.key_start));
        endTime = (ValidatedPreference) findPreference(getString(R.string.key_end));

        rate.setErrorListener(this);
        overtimeRate.setErrorListener(this);
        startTime.setErrorListener(this);
        endTime.setErrorListener(this);
    }

    @Override
    public void error(String message) {
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Activity activity = getActivity();
        Context context = activity.getBaseContext();

        if (key.equals(getString(R.string.key_notifications))) {
            if (mSavedSettings.getNotifications()) {
                Intent intent = new Intent(context, NotificationService.class);
                context.startService(intent);
            } else {
                Intent intent = new Intent(context, NotificationService.class);
                context.stopService(intent);
            }
        }
    }

    @Override
    public void onDestroy() {
        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }
}
