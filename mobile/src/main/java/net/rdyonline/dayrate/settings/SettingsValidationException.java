package net.rdyonline.dayrate.settings;

/**
 * Created by Ben Pearson (RdyDev) on 30/04/15.
 */
public class SettingsValidationException extends RuntimeException {

    private String friendlyMessage;

    public SettingsValidationException(String friendlyMessage) {
        this.friendlyMessage = friendlyMessage;
    }

    public String getFriendlyMessage() {
        return friendlyMessage;
    }

}
