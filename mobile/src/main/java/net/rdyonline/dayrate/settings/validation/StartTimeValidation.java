package net.rdyonline.dayrate.settings.validation;

import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.settings.SavedSettings;
import net.rdyonline.dayrate.settings.SettingsValidationException;

import android.content.Context;

/**
 * Created by Ben Pearson (RdyDev) on 22/11/15.
 */
public class StartTimeValidation {

    private Context context;

    public StartTimeValidation(Context context) {
        this.context = context;
    }

    public boolean isValid(SavedSettings savedSettings, Time startTime) {
        boolean valid = savedSettings.getEnd().isAfter(startTime);

        if (!valid) {
            throw new SettingsValidationException(context.getString(R.string.validation_start_time_end_time));
        }

        return valid;
    }
}
