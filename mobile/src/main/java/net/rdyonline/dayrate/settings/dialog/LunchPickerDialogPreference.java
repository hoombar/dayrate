package net.rdyonline.dayrate.settings.dialog;

import net.rdyonline.dayrate.QuarterHour;
import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.settings.SavedSettings;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;

/**
 * Created by Ben Pearson (RdyDev) on 28/04/15.
 */
public class LunchPickerDialogPreference extends DialogPreference {

    private SavedSettings savedSettings;
    private NumberPicker numberPicker;
    private String selected;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LunchPickerDialogPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public LunchPickerDialogPreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);
        init();
    }

    public LunchPickerDialogPreference(Context ctxt, AttributeSet attrs) {
        super(ctxt, attrs);
        init();
    }

    public LunchPickerDialogPreference(Context context) {
        this(context, null);
    }

    private void init() {
        setPositiveButtonText(R.string.set);
        setNegativeButtonText(R.string.cancel);

        savedSettings = savedSettings();
    }

    @Override
    protected View onCreateDialogView() {
        numberPicker = new NumberPicker(getContext());

        numberPicker.setDisplayedValues(QuarterHour.increments);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(QuarterHour.increments.length - 1);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        int selectedValue = 0;
        String savedString = savedSettings.getLunch().toString();
        for (int i = 0; i < QuarterHour.increments.length; i++) {
            if (savedString.equals(QuarterHour.increments[i])) {
                selectedValue = i;
                break;
            }
        }
        numberPicker.setValue(selectedValue);
        numberPicker.invalidate();

        return numberPicker;
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        if (savedSettings == null) init();

        return savedSettings.getLunch().toString();
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            selected = QuarterHour.increments[numberPicker.getValue()];

            setSummary(getSummary());
            if (callChangeListener(selected)) {
                savedSettings.setLunch(QuarterHour.fromString(selected));
                notifyChanged();
            }
        }
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        if (defaultValue == null) {
            selected = getDefault().toString();
        } else {
            selected = (String) defaultValue;
        }

        if (restoreValue) {
            selected = savedSettings.getLunch().toString();
        }

        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary() {
        return savedSettings.getLunch().toString();
    }

    private String getDefault() {
        return QuarterHour.one.toString();
    }
}
