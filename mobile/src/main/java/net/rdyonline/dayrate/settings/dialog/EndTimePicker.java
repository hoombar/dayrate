package net.rdyonline.dayrate.settings.dialog;

import net.rdyonline.dayrate.Time;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Ben Pearson (RdyDev) on 22/11/15.
 */
public class EndTimePicker extends TimePickerDialogPreference {

    public EndTimePicker(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public EndTimePicker(final Context ctxt, final AttributeSet attrs, final int defStyle) {
        super(ctxt, attrs, defStyle);
    }

    public EndTimePicker(final Context ctxt, final AttributeSet attrs) {
        super(ctxt, attrs);
    }

    @Override
    protected boolean setTime(final Time newValue) {
        return savedSettings.setEnd(newValue);
    }

    @Override
    protected Time getTime() {
        return savedSettings.getEnd();
    }
}
