package net.rdyonline.dayrate.settings.dialog;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.util.AttributeSet;

import net.rdyonline.dayrate.settings.SavedSettings;
import net.rdyonline.dayrate.settings.SettingsValidationException;
import net.rdyonline.dayrate.settings.validation.ValidatedPreference;

import static net.rdyonline.dayrate.modules.SettingsModule.defaultSettings;
import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;
import static net.rdyonline.dayrate.modules.ValidationModule.overtimeRateValidation;
import static net.rdyonline.dayrate.modules.ValidationModule.rateValidation;

/**
 * Created by Ben Pearson (RdyDev) on 30/04/15.
 */
public class RateDialogPreference extends EditTextPreference implements ValidatedPreference {

    SavedSettings mSavedSettings;
    SavedSettings.OnValidationError listener;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RateDialogPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    public RateDialogPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public RateDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RateDialogPreference(Context context) {
        super(context);
        init();
    }

    private void init() {
        mSavedSettings = savedSettings();
        setOnPreferenceChangeListener(this);
    }

    @Override
    protected String getPersistedString(String defaultReturnValue) {
        return Double.toString(mSavedSettings.getRate());
    }

    @Override
    protected boolean persistString(String value) {
        try {
            double rate = Double.parseDouble(value);
            mSavedSettings.setRate(rate);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    @Override
    public CharSequence getSummary() {
        return Double.toString(mSavedSettings.getRate());
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            setSummary(getSummary());
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        try {
            double rate = Double.parseDouble((String) newValue);
            if (mSavedSettings.setRate(rate)) {
                return true;
            }
        } catch (NumberFormatException e) {
            String rate = Double.toString(mSavedSettings.getRate());
            listener.error("Rate must be numeric. e.g. " + rate);
        } catch (SettingsValidationException e) {
            listener.error(e.getFriendlyMessage());
        }

        return false;
    }

    @Override
    public void setErrorListener(SavedSettings.OnValidationError listener) {
        this.listener = listener;
    }
}
