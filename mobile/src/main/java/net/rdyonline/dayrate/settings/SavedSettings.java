package net.rdyonline.dayrate.settings;

import android.content.Context;
import android.content.SharedPreferences;

import net.rdyonline.dayrate.QuarterHour;
import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.modules.SettingsModule;
import net.rdyonline.dayrate.settings.validation.EndTimeValidation;
import net.rdyonline.dayrate.settings.validation.OvertimeRateValidation;
import net.rdyonline.dayrate.settings.validation.RateValidation;
import net.rdyonline.dayrate.settings.validation.StartTimeValidation;

import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * Adapter for talking to shared preferences through a common interface
 * <p/>
 * Created by Ben Pearson (RdyDev) on 27/04/15.
 */
public class SavedSettings implements Settings {

    // keys
    static String KEY_DEFAULTS_SET ;
    static String KEY_RATE;
    static String KEY_NOTIFICATIONS;
    static String KEY_NOTIFICATION_TEMPORARY_DISMISS;
    static String KEY_START;
    static String KEY_LUNCH_DURATION;
    static String KEY_END;
    static String KEY_OVERTIME_RATE;
    static String KEY_OVERTIME;

    private final Settings defaultSettings;

    private SharedPreferences sharedPreferences;
    private RateValidation rateValidation;
    private OvertimeRateValidation overtimeRateValidation;
    private StartTimeValidation startTimeValidation;
    private EndTimeValidation endTimeValidation;

    public SavedSettings(Context context, RateValidation rateValidation, OvertimeRateValidation overtimeRateValidation, Settings defaultSettings, final StartTimeValidation startTimeValidation, final EndTimeValidation endTimeValidation) {
        this(context, rateValidation, overtimeRateValidation, defaultSettings, SettingsModule.sharedPreferences(), startTimeValidation, endTimeValidation);

        setDefaultsIfBlank();
    }

    SavedSettings(Context context, RateValidation rateValidation, OvertimeRateValidation overtimeRateValidation, Settings defaultSettings, SharedPreferences sharedPreferences, final StartTimeValidation startTimeValidation, final EndTimeValidation endTimeValidation) {
        this.sharedPreferences = sharedPreferences;
        this.rateValidation = rateValidation;
        this.overtimeRateValidation = overtimeRateValidation;
        this.defaultSettings = defaultSettings;
        this.startTimeValidation = startTimeValidation;
        this.endTimeValidation = endTimeValidation;

        KEY_DEFAULTS_SET = context.getString(R.string.key_defaults_set);
        KEY_RATE = context.getString(R.string.key_rate);
        KEY_NOTIFICATIONS = context.getString(R.string.key_notifications);
        KEY_NOTIFICATION_TEMPORARY_DISMISS = context.getString(R.string.key_notification_temporary_dismiss);
        KEY_START = context.getString(R.string.key_start);
        KEY_LUNCH_DURATION = context.getString(R.string.key_lunch_duration);
        KEY_END = context.getString(R.string.key_end);
        KEY_OVERTIME = context.getString(R.string.key_overtime);
        KEY_OVERTIME_RATE = context.getString(R.string.key_overtime_rate);
    }

    void setDefaultsIfBlank() {
        boolean defaultSet = sharedPreferences.getBoolean(KEY_DEFAULTS_SET, false);

        if (!defaultSet) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(KEY_DEFAULTS_SET, true);
            editor.commit();

            setRate(defaultSettings.getRate());
            setNotifications(defaultSettings.getNotifications());
            setStart(defaultSettings.getStart());
            setLunch(defaultSettings.getLunch());
            setEnd(defaultSettings.getEnd());
            setOvertimeRate(defaultSettings.getOvertimeRate());
            setOvertime(defaultSettings.getOvertime());
        }
    }

    public boolean setRate(double rate) {
        if (rateValidation.isValid(rate)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(KEY_RATE, Double.doubleToLongBits(rate));
            editor.commit();
            return true;
        }

        return false;
    }

    @Override
    public double getRate() {
        long savedValue = sharedPreferences.getLong(KEY_RATE, Double.doubleToLongBits(defaultSettings.getRate()));

        return Double.longBitsToDouble(savedValue);
    }

    public void setNotifications(boolean notify) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_NOTIFICATIONS, notify);
        editor.commit();
    }

    public void setNotificationDismissed(DateTime time) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(KEY_NOTIFICATION_TEMPORARY_DISMISS, time.getMillis());
        editor.commit();
    }

    @Override
    public boolean getNotifications() {
        boolean show = sharedPreferences.getBoolean(KEY_NOTIFICATIONS, defaultSettings.getNotifications());

        if (show) {
            show = !dismissedToday();
        }

        return show;
    }

    private boolean dismissedToday() {
        Long lastDismissTime = sharedPreferences.getLong(KEY_NOTIFICATION_TEMPORARY_DISMISS, 0);

        if (lastDismissTime == 0) return false;

        DateTime whenDismissed = new DateTime(lastDismissTime);
        DateTime now = DateTime.now();

        boolean dismissedToday = whenDismissed.getYear() == now.getYear();
        dismissedToday &= whenDismissed.getMonthOfYear() == now .getMonthOfYear();
        dismissedToday &= whenDismissed.getDayOfMonth() == now.getDayOfMonth();

        return dismissedToday;
    }

    public boolean setStart(Time start) {
        if (startTimeValidation.isValid(this, start)) {
            long millis = start.toMillis();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(KEY_START, millis);
            editor.commit();

            return true;
        }

        return false;
    }

    public Time getStart() {
        long value = sharedPreferences.getLong(KEY_START, defaultSettings.getStart().toMillis());

        return Time.fromMillis(value);
    }

    public void setLunch(QuarterHour quarterHour) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(KEY_LUNCH_DURATION, quarterHour.getValue());
        editor.commit();
    }

    @Override
    public QuarterHour getLunch() {
        return QuarterHour.fromValue(sharedPreferences.getLong(KEY_LUNCH_DURATION, defaultSettings.getLunch().getValue()));
    }

    public boolean setEnd(Time end) {
        if (endTimeValidation.isValid(this, end)) {
            long millis = end.toMillis();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(KEY_END, millis);
            editor.commit();

            return true;
        }

        return false;
    }

    @Override
    public Time getEnd() {
        long value = sharedPreferences.getLong(KEY_END, defaultSettings.getEnd().toMillis());

        return Time.fromMillis(value);
    }

    public boolean setOvertimeRate(double rate) {
        if (overtimeRateValidation.isValid(rate)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(KEY_OVERTIME_RATE, Double.doubleToLongBits(rate));
            editor.commit();
            return true;
        }

        return false;
    }

    public void setOvertime(boolean overtime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_OVERTIME, overtime);
        editor.commit();
    }

    @Override
    public double getOvertimeRate() {
        long savedValue = sharedPreferences.getLong(KEY_OVERTIME_RATE, Double.doubleToLongBits(defaultSettings.getOvertimeRate()));

        return Double.longBitsToDouble(savedValue);
    }

    @Override
    public boolean getOvertime() {
        return sharedPreferences.getBoolean(KEY_OVERTIME, defaultSettings.getOvertime());
    }
}
