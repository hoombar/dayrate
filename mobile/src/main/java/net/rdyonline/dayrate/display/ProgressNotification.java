package net.rdyonline.dayrate.display;

import net.rdyonline.dayrate.CoinageCalculator;
import net.rdyonline.dayrate.DayRateApplication;
import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.modules.ApplicationModule;
import net.rdyonline.dayrate.settings.SavedSettings;

import org.joda.time.DateTime;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * When the app isn't actively in the foreground, a notification is displayed to allow quick
 * entry in to the app, but also to provide live progress of the day so far.
 * Once the day has finished, as long as overtime isn't on, the notification will disappear.
 * <p/>
 * To start with, the notification won't start automatically when the day starts, but will be
 * triggered from a launch of the app
 * <p/>
 * Created by Ben Pearson (RdyDev) on 23/05/15.
 */
public class ProgressNotification {

    public static final int NOTIFICATION_ID = 0x1;

    private String title;
    private CoinageCalculator calculator;
    private SavedSettings mSavedSettings;

    public ProgressNotification(String title, CoinageCalculator calculator, SavedSettings savedSettings) {
        this.title = title;
        this.calculator = calculator;
        this.mSavedSettings = savedSettings;
    }

    /***
     * Decide what to do when the notification is tapped. Progress should link through to the
     * main screen that contains the progress of the day and other information
     *
     * @param context Intent requires a context
     * @return constructed PendingIntent with appropriate flags and targets
     */
    private PendingIntent getPendingIntent(Context context) {
        int pFlags = PendingIntent.FLAG_CANCEL_CURRENT;
        int iFlags = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        Intent target = new Intent(context, MainActivity.class);
        target.addFlags(iFlags);
        return PendingIntent.getActivity(context, 0, target, pFlags);
    }

    /***
     * Build a notification contining interesting information about the current state of
     * progress through the day; e.g. how much, how long, etc
     *
     * @param c The notification build requires context
     * @return a fully constructed notification, ready for display
     */
    public Notification getNotification(Context c) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(c);
        builder.setSmallIcon(R.drawable.ic_stat_dollar103);
        builder.setContentTitle(title);
        PendingIntent pendingIntent = getPendingIntent(c);
        builder.setContentIntent(pendingIntent);
        builder.setOngoing(true);
        builder.setWhen(System.currentTimeMillis());
        builder.setAutoCancel(true);

        addDismissButton(builder);

        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        boolean overtime = mSavedSettings.getOvertime();
        double coins = calculator.getTotalCoins(DateTime.now(), overtime);

        builder.setContentText(formatter.format(coins));

        return builder.build();
    }

    private void addDismissButton(NotificationCompat.Builder builder) {
        Context context = ApplicationModule.context();
        Intent updateServiceIntent = new Intent(DayRateApplication.DISMISS_NOTIFICATION);
        PendingIntent pendingUpdateIntent = PendingIntent.getBroadcast(ApplicationModule.context(),
                NOTIFICATION_ID, updateServiceIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        String title = context.getString(R.string.notification_action_dismiss);

        NotificationCompat.Action dismiss = new NotificationCompat.Action(R.drawable.ic_dismiss, title, pendingUpdateIntent);
        builder.addAction(dismiss);
    }

}
