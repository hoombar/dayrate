package net.rdyonline.dayrate.display;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.rdyonline.dayrate.CoinageCalculator;
import net.rdyonline.dayrate.DayProgress;
import net.rdyonline.dayrate.OvertimeProgress;
import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.Rate;
import net.rdyonline.dayrate.Schedule;
import net.rdyonline.dayrate.notification.NotificationService;
import net.rdyonline.dayrate.settings.SettingsActivity;
import net.rdyonline.dayrate.settings.SavedSettings;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static net.rdyonline.dayrate.modules.SettingsModule.defaultSettings;
import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;
import static net.rdyonline.dayrate.modules.ValidationModule.overtimeRateValidation;
import static net.rdyonline.dayrate.modules.ValidationModule.rateValidation;

public class MainActivity extends AppCompatActivity {

    private final int PROGRESS_ANIMATION_DURATION = 500;

    @InjectView(R.id.progress_time_done)
    ProgressBar pbDayProgress;
    @InjectView(R.id.progress_overtime_done)
    ProgressBar pbOvertimeProgress;
    @InjectView(R.id.current_pennies)
    TextView txtCoins;

    @InjectView(R.id.main_txt_total_hours)
    TextView txtWorkingHourTotal;
    @InjectView(R.id.main_txt_working_hours)
    TextView txtWorkingHours;
    @InjectView(R.id.main_txt_total_rate)
    TextView txtTotalRate;
    @InjectView(R.id.main_txt_hour_rate)
    TextView txtHourRate;
    @InjectView(R.id.main_txt_overtime)
    TextView txtOvertime;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    private SavedSettings mSavedSettings;
    private Schedule schedule;
    private DayProgress dayProgress;
    private OvertimeProgress overtimeProgress;
    private Rate rate;
    private CoinageCalculator coinageCalculator;

    private DecimalFormat decimalFormat = new DecimalFormat("#.00");
    private final int SECOND = 1000;

    final Handler handler = new Handler();

    /***
     * As you are watching the screen, the progres should tick over and the value of money made
     * so far should go up
     */
    private Runnable updateUiThread = new Runnable() {

        @Override
        public void run() {
            DateTime dateTime = DateTime.now();
            int progress = getProgress(dateTime);
            long elapsed = dayProgress.getOvertimeSeconds(dateTime);
            boolean overtime = mSavedSettings.getOvertime();

            double coins = coinageCalculator.getTotalCoins(dateTime, overtime);

            // If no money has been made yet, display the day rate
            if (coins == 0) coins = rate.perDay();

            pbDayProgress.setProgress(progress);
            pbOvertimeProgress.setProgress(overtimeProgress.getProgress(elapsed));

            txtCoins.setText(decimalFormat.format(coins));
            handler.postDelayed(this, SECOND / 2);
        }
    };

    /***
     * Round up when getting progress to optimize display
     *
     * @param dateTime required to know progress
     * @return Ceiling version of progress
     */
    private int getProgress(DateTime dateTime) {
        return (int) Math.ceil(dayProgress.getProgress(dateTime));
    }

    public MainActivity() {
        this(savedSettings());
    }

    public MainActivity(SavedSettings savedSettings) {
        mSavedSettings = savedSettings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
        }
    }

    @Override
    protected void attachBaseContext(final Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = new Intent(this, NotificationService.class);
        stopService(intent);

        schedule = new Schedule(mSavedSettings);
        dayProgress = new DayProgress(schedule);
        overtimeProgress = new OvertimeProgress(schedule);
        rate = new Rate(mSavedSettings.getRate(), schedule, mSavedSettings.getOvertimeRate());
        coinageCalculator = new CoinageCalculator(schedule, dayProgress, rate);

        DateTime now = DateTime.now();
        long overtimeSeconds = dayProgress.getOvertimeSeconds(now);
        final int progress = dayProgress.getProgress(now);
        final int overtimeProgress = this.overtimeProgress.getProgress(overtimeSeconds);
        if (progress > 0) {
            animateProgress(progress, overtimeProgress);
        } else {
            handler.postDelayed(updateUiThread, 0);
        }

        setHours();
        setRate();
        setOvertime();
    }

    /***
     * {@link ObjectAnimator} can not animate on a {@link TextView}, but it can animate on a
     * property that takes a float value, so this acts as a wrapper to allow easy updating
     * of the text in an animation by using a {@link ObjectAnimator}
     */
    class TextAnimationWrapper {

        @SuppressWarnings("unused")
        public void setText(float value) {
            txtCoins.setText(decimalFormat.format(value));
        }

    }

    /**
     * When starting or resming the app, the progress should spin around
     *
     * @param progress         how far through we are
     * @param overtimeProgress if in overtime period, how far through
     */
    private void animateProgress(final int progress, final int overtimeProgress) {
        pbDayProgress.setProgress(0);
        pbOvertimeProgress.setProgress(0);
        txtCoins.setText(decimalFormat.format(0.0));

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                float coins = (float) coinageCalculator.getCurrentCoins(DateTime.now());

                int delay = PROGRESS_ANIMATION_DURATION;
                animateCoinValue(0, coins, delay);
                animateProgressBar(pbDayProgress, progress, delay);
                animateProgressBar(pbOvertimeProgress, overtimeProgress, delay * 2);

                if (mSavedSettings.getOvertime()) {
                    float extraCoins = (float) coinageCalculator.getTotalCoins(DateTime.now(), true);
                    animateCoinValue(coins, extraCoins, delay * 2);
                }

                // wait for the animation to finish before starting update of progress
                handler.postDelayed(updateUiThread, delay * 3);

            }
        }, 100);
    }

    /**
     * When starting or resuming the app, the amount earned so far should animate from 0 to the
     * current amount
     *
     * @param start start amount to animate from
     * @param end   the resultant amount
     * @param delay how long to wait before trigerring the animation
     */
    private void animateCoinValue(float start, float end, long delay) {
        TextAnimationWrapper wrapper = new TextAnimationWrapper();
        ObjectAnimator animation = ObjectAnimator.ofFloat(wrapper, "text", start, end);
        animation.setDuration(PROGRESS_ANIMATION_DURATION);
        animation.setStartDelay(delay);
        animation.start();
    }

    private void animateProgressBar(ProgressBar progressBar, int value, long delay) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", value);
        animation.setDuration(PROGRESS_ANIMATION_DURATION);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setStartDelay(delay);
        animation.start();
    }

    private void setHours() {
        double totalHours = schedule.getWorkingDuration(true).getStandardMinutes() / (double) 60;

        String hours = decimalFormat.format(totalHours);
        txtWorkingHourTotal.setText(hours + " HRS");
        txtWorkingHours.setText("(" + schedule.toString() + ")");
    }

    /**
     * Update the rate display based on the users settings.
     * Display in the current users currency, according to device settings
     */
    private void setRate() {
        double totalRate = rate.perDay();
        double hourRate = rate.perHour(true);

        NumberFormat moneyFormatter = NumberFormat.getCurrencyInstance();

        txtTotalRate.setText(moneyFormatter.format(totalRate));
        txtHourRate.setText("(" + moneyFormatter.format(hourRate) + "/HR)");
    }

    /**
     * Update the overtime label with the current rate of overtime
     */
    private void setOvertime() {
        int otVisibility = mSavedSettings.getOvertime() ? View.VISIBLE : View.GONE;
        pbOvertimeProgress.setVisibility(otVisibility);

        txtOvertime.setText("Overtime: " + Double.toString(rate.getOvertime()) + "x");
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(updateUiThread);

        /**
         * When the activity moves to the background, the notification should show.
         * Note that, at the moment, this is the only entry point to the notification, so they
         * will not see a notification unless they have come in to the app and navigated away.
         * This is intentional behaviour.
         */
        if (mSavedSettings.getNotifications()) {
            Intent intent = new Intent(this, NotificationService.class);
            startService(intent);
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent showSettings = new Intent(this, SettingsActivity.class);
            startActivity(showSettings);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
