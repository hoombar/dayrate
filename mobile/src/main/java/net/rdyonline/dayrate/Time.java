package net.rdyonline.dayrate;

import org.joda.time.DateTime;

/**
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class Time implements Comparable<Time> {

    private int hour;
    private int minute;
    private int second;

    public Time(DateTime dateTime) {
        this(dateTime.getHourOfDay(), dateTime.getMinuteOfHour(), dateTime.getSecondOfMinute());
    }

    public Time(int hour, int minute) {
        this(hour, minute, 0);
    }

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    /**
     * @return total number of milliseconds
     */
    public long toMillis() {
        return toDateTime().getMillis();
    }

    public static Time fromMillis(long millis) {
        DateTime dateTime = new DateTime(millis);
        Time time = new Time(dateTime);

        return time;
    }

    /**
     * Returns a valid {@link org.joda.time.DateTime} with only the hour and minute populated.
     * The year, month and day are 1970/1/1 and the second are zeroed
     *
     * @return
     */
    public DateTime toDateTime() {
        return new DateTime(1970, 1, 1, getHour(), getMinute(), getSecond());
    }

    public boolean isAfter(Time another) {
        return compareTo(another) > 0;
    }

    public boolean isBefore(Time another) {
        return compareTo(another) < 0;
    }

    @Override
    public int compareTo(Time another) {
        if (getHour() > another.getHour()) return 1;
        if (getHour() < another.getHour()) return -1;
        if (getMinute() > another.getMinute()) return 1;
        if (getMinute() < another.getMinute()) return -1;
        if (getSecond() > another.getSecond()) return 1;
        if (getSecond() < another.getSecond()) return -1;

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Time)) return false;
        Time other = (Time) o;

        return toDateTime().equals(other.toDateTime());
    }

    @Override
    public int hashCode() {
        return toDateTime().hashCode();
    }

    public String asString() {
        return getHour() + ":" + String.format("%02d", getMinute());
    }
}
