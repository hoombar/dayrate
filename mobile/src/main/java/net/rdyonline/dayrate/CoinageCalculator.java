package net.rdyonline.dayrate;

import org.joda.time.DateTime;

/**
 * How much sucky sucky five dollah has been ker-chinged today
 * <p/>
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class CoinageCalculator {

    private static final String TAG = CoinageCalculator.class.getSimpleName();

    private Rate rate;
    private DayProgress dayProgress;
    private Schedule schedule;

    public CoinageCalculator(Schedule schedule, DayProgress dayProgress, Rate rate) {
        this.schedule = schedule;
        this.rate = rate;
        this.dayProgress = dayProgress;
    }

    public double getTotalCoins(DateTime dateTime, boolean overtime) {
        double dayCoins = getCurrentCoins(dateTime);

        if (!overtime) {
            return dayCoins;
        } else {
            return dayCoins + getOvertimeCoins(dateTime);
        }
    }

    public double getCurrentCoins(DateTime time) {
        double secondsSoFar = dayProgress.getSecondsWorked(time);
        double hourRate = rate.perHour(false);

        double secondRate = hourRate / (60 * 60);
        double pennies = secondsSoFar * secondRate;

        return pennies;
    }

    public double getOvertimeCoins(DateTime dateTime) {
        long additionalSeconds = dayProgress.getOvertimeSeconds(dateTime);

        if (additionalSeconds <= 0) return 0;

        double hourRate = rate.perHour(true) * rate.getOvertime();

        double secondRate = hourRate / (60 * 60);
        double pennies = additionalSeconds * secondRate;

        return pennies;
    }

}
