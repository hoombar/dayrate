package net.rdyonline.dayrate.notification;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import net.rdyonline.dayrate.CoinageCalculator;
import net.rdyonline.dayrate.DayProgress;
import net.rdyonline.dayrate.R;
import net.rdyonline.dayrate.Rate;
import net.rdyonline.dayrate.Schedule;
import net.rdyonline.dayrate.Time;
import net.rdyonline.dayrate.display.ProgressNotification;
import net.rdyonline.dayrate.settings.SavedSettings;

import org.joda.time.DateTime;

import java.util.Timer;
import java.util.TimerTask;

import static net.rdyonline.dayrate.modules.SettingsModule.defaultSettings;
import static net.rdyonline.dayrate.modules.SettingsModule.savedSettings;
import static net.rdyonline.dayrate.modules.ValidationModule.overtimeRateValidation;
import static net.rdyonline.dayrate.modules.ValidationModule.rateValidation;

/**
 * When DayRate is minimised, a notification should appear to tell the user how much they have
 * made so far without launching the app.
 * <p/>
 * The {@link NotificationService} will decide whether or not a notification should be shown.
 * <p/>
 * Created by Ben Pearson (RdyDev) on 02/06/15.
 */
public class NotificationService extends Service {

    private Schedule schedule;
    private DayProgress dayProgress;
    private SavedSettings mSavedSettings;
    private ProgressNotification progressNotification;
    private Timer timer = new Timer();

    public NotificationService() {
        this(savedSettings());
    }

    public NotificationService(SavedSettings savedSettings) {
        mSavedSettings = savedSettings;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // TODO(benp) Rx all the things!
        schedule = new Schedule(mSavedSettings);
        dayProgress = new DayProgress(schedule);
        Rate rate = new Rate(mSavedSettings.getRate(), schedule, mSavedSettings.getOvertimeRate());
        CoinageCalculator coinageCalculator = new CoinageCalculator(schedule, dayProgress, rate);
        String appName = getString(R.string.app_name);

        progressNotification = new ProgressNotification(appName, coinageCalculator, mSavedSettings);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                requestNotificationDisplay();
            }
        }, 0, 1000);

        return super.onStartCommand(intent, flags, startId);
    }

    /***
     * If the current time falls within the window that a notification should be displayed, this
     * will handle starting a foreground notification.
     * <p/>
     * If the time is before the start of the day, the notification should not be shown.
     * If the time is after working hours, but overtime is on, it should show.
     */
    private void requestNotificationDisplay() {
        if (shouldShowNotification()) {
            startForeground(ProgressNotification.NOTIFICATION_ID, progressNotification.getNotification(this));
        } else {
            // in case there is already a notification running, it needs to be destroyed
            onDestroy();
        }
    }

    /***
     * Notifications should be shown if various conditions are met, such as:
     * - we are within the working day
     * - we are after working day, but overtime is on
     *
     * @return
     */
    private boolean shouldShowNotification() {
        DateTime now = DateTime.now();
        boolean afterStart = dayProgress.hasDayStarted(new Time(now));
        boolean beforeEnd = !dayProgress.hasDayFinished(new Time(now));

        boolean show = mSavedSettings.getNotifications();
        if (show) {
            // in the working day
            show = afterStart && beforeEnd;

            if (!show) {
                // not in working day
                // if after end of day, is overtime on?
                show = !beforeEnd && mSavedSettings.getOvertime();
            }
        }

        return show;
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        stopForeground(true);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new IllegalStateException("Binding has not been set up for the service");
    }
}
