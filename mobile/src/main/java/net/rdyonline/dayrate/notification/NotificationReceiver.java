package net.rdyonline.dayrate.notification;

import net.rdyonline.dayrate.modules.SettingsModule;
import net.rdyonline.dayrate.settings.SavedSettings;

import org.joda.time.DateTime;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Ben Pearson (RdyDev) on 16/10/15.
 */
public class NotificationReceiver extends BroadcastReceiver {

    private SavedSettings savedSettings;

    @SuppressWarnings("unused")
    public NotificationReceiver() {
        this(SettingsModule.savedSettings());
    }

    public NotificationReceiver(SavedSettings savedSettings) {
        this.savedSettings = savedSettings;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        savedSettings.setNotificationDismissed(DateTime.now());
    }
}
