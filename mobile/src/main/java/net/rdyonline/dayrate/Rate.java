package net.rdyonline.dayrate;

/**
 * Created by Ben Pearson (RdyDev) on 23/04/15.
 */
public class Rate {

    private double dayPay;
    private Schedule schedule;
    private double overtime;

    public Rate(double dayPay, Schedule schedule, double overtime) {
        this.dayPay = dayPay;
        this.schedule = schedule;
        this.overtime = overtime;
    }

    public double perDay() {
        return dayPay;
    }

    public double perHour(boolean excludeLunch) {
        return perMinute(excludeLunch) * 60;
    }

    public double perMinute(boolean excludeLunch) {
        return perSecond(excludeLunch) * 60;
    }

    public double perSecond(boolean excludeLunch) {
        long seconds = schedule.getWorkingDuration(excludeLunch).getStandardSeconds();

        return dayPay / (double) seconds;
    }

    public double getOvertime() {
        return overtime;
    }
}
