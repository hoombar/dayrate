package net.rdyonline.dayrate.modules;

import net.rdyonline.dayrate.settings.validation.OvertimeRateValidation;
import net.rdyonline.dayrate.settings.validation.RateValidation;

import static net.rdyonline.dayrate.modules.ApplicationModule.context;

/**
 * Created by Ben Pearson (RdyDev) on 16/09/15.
 */
public class ValidationModule {

    public static final RateValidation rateValidation() {
        return new RateValidation(context());
    }

    public static final OvertimeRateValidation overtimeRateValidation() {
        return new OvertimeRateValidation(context());
    }

}
