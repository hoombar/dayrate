package net.rdyonline.dayrate.modules;

import net.rdyonline.dayrate.settings.DefaultSettings;
import net.rdyonline.dayrate.settings.SavedSettings;
import net.rdyonline.dayrate.settings.validation.EndTimeValidation;
import net.rdyonline.dayrate.settings.validation.OvertimeRateValidation;
import net.rdyonline.dayrate.settings.validation.RateValidation;
import net.rdyonline.dayrate.settings.validation.StartTimeValidation;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static net.rdyonline.dayrate.modules.ApplicationModule.context;

/**
 * Created by Ben Pearson (RdyDev) on 16/09/15.
 */
public class SettingsModule {

    private static SavedSettings savedSettings;

    public static DefaultSettings defaultSettings() {
        return new DefaultSettings();
    }

    public static SharedPreferences sharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context());
    }

    public static SavedSettings savedSettings() {
        if (savedSettings == null) {
            synchronized (SettingsModule.class) {
                if (savedSettings == null) {
                    savedSettings = new SavedSettings(context(), rateValidation(), overtimeRateValidation(), defaultSettings(), startTimeValidation(), endTimeValidation());
                }
            }
        }

        return savedSettings;
    }

    private static RateValidation rateValidation() {
        return new RateValidation(context());
    }

    private static OvertimeRateValidation overtimeRateValidation() {
        return new OvertimeRateValidation(context());
    }

    private static StartTimeValidation startTimeValidation() {
        return new StartTimeValidation(ApplicationModule.context());
    }

    private static EndTimeValidation endTimeValidation() {
        return new EndTimeValidation(ApplicationModule.context());
    }
}
