package net.rdyonline.dayrate.modules;

import android.content.Context;

/**
 * Created by Ben Pearson (RdyDev) on 16/09/15.
 */
public class ApplicationModule {

    private static Context applicationContext;

    public static void init(Context context) {
        applicationContext = context;
    }

    public static final Context context() {
        return applicationContext;
    }
}
